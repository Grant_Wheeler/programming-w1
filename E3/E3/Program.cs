﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E3
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "";

            Console.WriteLine("What is your name?");
            myName = (Console.ReadLine());

            Console.WriteLine($"Thank you {myName}");
        }
    }
}
