﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E5
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 4;
            var b = 9;

            Console.WriteLine("a + b = 13");

            Console.WriteLine($"{a} + {b} = {a + b}");

            //I did things this way because it was a fluke and it worked
        }
    }
}
